import { Box, Image, Text, SimpleGrid, Flex } from "@chakra-ui/react";
import { Link } from "react-router-dom";

const DevisNumItems = ({ children }) =>
  children.map((value, index) => {
    return <Text>{value}</Text>;
  });

export default function BlocClient(props) {
  return (
    <Link to={{ pathname: `/FicheClient/${props.id}` }}>
      <Box
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        _hover={{ backgroundColor: "teal.300", color: "white" }}
      >
        <SimpleGrid columns={2} spacing={5}>
          <Box>
            <Image height="80%" src={props.image} alt="Segun Adebayo" />
          </Box>
          <Box paddingTop="20%" fontSize={{ base: "md", md: "md", xl: "xl" }}>
            <Flex>
              <Text as="b">Nom : </Text>
              <Text textAlign="left">{props.nom}</Text>
            </Flex>
            <Flex>
              <Text as="b">Prénom : </Text>
              <Text>{props.prenom}</Text>
            </Flex>
            <Flex>
              <Text as="b">Ville : </Text>
              <Text>{props.ville}</Text>
            </Flex>

            <Text as="b">Devis : </Text>
            <Box style={{ maxHeight: "10vh", overflow: "auto" }}>
              <DevisNumItems>{props.devis ? props.devis : []}</DevisNumItems>
            </Box>
          </Box>
        </SimpleGrid>
      </Box>
    </Link>
  );
}
