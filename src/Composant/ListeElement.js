import React from "react";
import { Link } from "react-router-dom";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Tfoot,
  TableCaption,
  Box,
} from "@chakra-ui/react";

const DataItems = ({ children }) => (
  <Tr _hover={{ color: "tomato" }}>
    {children.map((value, index) => {
      //test si url en attribut de l'object afin de créer le link associé
      if (children[0][1] != null) {
        return index > 1 ? (
          <Td fontSize={{ base: "xs", md: "xs", xl: "md" }} key={index}>
            <Link to={{ pathname: children[0][1], state: children[1][1] }}>
              {value[1]}
            </Link>
          </Td>
        ) : null;
      } else {
        return index > 1 ? <Td key={index}>{value[1]}</Td> : null;
      }
    })}
  </Tr>
);

export default function ListeElement(props) {
  return (
    <Box
      style={{ maxHeight: "70vh", overflow: "auto" }}
      bg="teal.50"
      borderRadius="xl"
    >
      <Table variant="striped" colorScheme="teal">
        <TableCaption>{props.dessousListe}</TableCaption>
        <Thead>
          <Tr>
            {props.nomColonne.map((value, index) => {
              return (
                <Th fontSize={{ base: "xs", md: "xs", xl: "md" }} key={index}>
                  {value}
                </Th>
              );
            })}
          </Tr>
        </Thead>
        <Tbody>
          {props.data.map((value) => {
            return <DataItems>{Object.entries(value)}</DataItems>;
          })}
        </Tbody>
        <Tfoot>
          <Tr>
            {props.nomColonne.map((value, index) => {
              return (
                <Th fontSize={{ base: "xs", md: "xs", xl: "md" }} key={index}>
                  {value}
                </Th>
              );
            })}
          </Tr>
        </Tfoot>
      </Table>
    </Box>
  );
}
