import React, { Component } from "react";
import { Box } from "@chakra-ui/react";
import * as THREE from "three";

class BlocThree extends Component {
  async componentDidMount() {
    const width = 600;
    const height = 250;

    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(45, width / height, 1, 10000);
    const renderer = new THREE.WebGLRenderer({ antialias: true });

    const loader = new THREE.ObjectLoader();

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open(
      "GET",
      `${process.env.REACT_APP_URL_BACKEND}/downloadProduit/${this.props.modelJson}.json`,
      false
    ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", `Bearer ${this.props.token}`);
    xmlHttp.send(null);
    var object = JSON.parse(xmlHttp.responseText);
    object = await loader.parse(object);

    camera.position.set(25, 10, 15);
    camera.lookAt(15, 7, 10);

    renderer.setClearColor("#d9dddc");
    renderer.setSize(width, height);
    scene.add(object);
    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    this.mount.appendChild(this.renderer.domElement);
    this.renderer.render(this.scene, this.camera);
  }

  render() {
    return (
      <Box
        ml={10}
        mt={5}
        height="100%"
        maxW="xl"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
      >
        <div
          ref={(mount) => {
            this.mount = mount;
          }}
        />
      </Box>
    );
  }
}

export default BlocThree;
