import React from "react";
import {
  Box,
  Image,
  Text,
  Button,
  CloseButton,
  useDisclosure,
} from "@chakra-ui/react";
import ModalDelete from "../Modal/ModalDelete";
import { Link as ReactLink } from "react-router-dom";

export default function BlocProduit(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const token = localStorage.getItem("token");
  const getPdf = () => {
    var URL = `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis/pdfreport/${props.devis}/produit/${props.id}`;
    fetch(URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.blob())
      .then((blob) => {
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement("a");
        a.href = url;
        a.download = "filename.pdf";
        document.body.appendChild(a);
        a.click();
        a.remove();
      });
  };
  return (
    <Box
      height="100%"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      backgroundColor={props.validation}
      _hover={{ backgroundColor: "teal.300", color: "white" }}
    >
      <CloseButton
        size="sm"
        backgroundColor="teal.500"
        color="white"
        onClick={onOpen}
      />
      <ModalDelete
        isOpen={isOpen}
        onClose={onClose}
        elementModel="Produit"
        nom={props.nom}
        id={props.id}
        token={token}
        reload={props.reload}
      />

      <Box>
        <ReactLink
          to={{
            pathname: `/DetailProduit/${props.id}/devis/${props.devis}`,
          }}
        >
          <Image width="100%" src={props.image} alt="Segun Adebayo" />
          <Box fontSize={{ base: "xs", md: "sm", xl: "lg" }} ml={2}>
            <Text>{`Nom : ${props.nom}`.substring(0, 17)}</Text>
            <Text>{`Gamme : ${props.gamme}`.substring(0, 17)}</Text>
            <Text>Prix : {props.prix} €</Text>
            <Text>Créé le : {props.cree}</Text>
            <Text>Maj le : {props.maj}</Text>
          </Box>
        </ReactLink>
        <Button
          fontSize={{ base: "xs", md: "xs", xl: "md" }}
          backgroundColor="teal.500"
          onClick={getPdf}
        >
          Générer dossier
        </Button>
      </Box>
    </Box>
  );
}
