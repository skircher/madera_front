import React from "react";
import { Box, Image, Text, SimpleGrid, Flex } from "@chakra-ui/react";
import { Link } from "react-router-dom";

export default function BlocDevis(props) {
  return (
    <Link to={{ pathname: `/DetailDevis/${props.id}` }}>
      <Box
        height="100%"
        maxW="sm"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        _hover={{ backgroundColor: "teal.300", color: "white" }}
      >
        <SimpleGrid columns={2} spacing={5}>
          <Box>
            <Image src={props.image} alt="Segun Adebayo" />
          </Box>
          <Box fontSize={{ base: "xs", md: "sm", xl: "md" }}>
            <Flex>
              <Text as="b">Devis : </Text>
              <Text>{props.devis}</Text>
            </Flex>
            <Flex>
              <Text as="b">Etat : </Text>
              <Text>{props.etat}</Text>
            </Flex>
            <Flex>
              <Text as="b">Nom : </Text>
              <Text>{props.nom}</Text>
            </Flex>
            <Flex>
              <Text as="b">Prénom : </Text>
              <Text>{props.prenom}</Text>
            </Flex>
            <Flex>
              <Text as="b">Ville : </Text>
              <Text>{props.ville}</Text>
            </Flex>
          </Box>
        </SimpleGrid>
      </Box>
    </Link>
  );
}
