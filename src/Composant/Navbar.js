import React from "react";
import { Box, Heading, Flex, Text, Button } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { BiUserX } from "react-icons/bi";
import { FaUserAlt } from "react-icons/fa";

const MenuItems = ({ children }) => (
  <Link to={{ pathname: children.url, state: children.state }}>
    <Text
      mt={{ base: 4, md: 0 }}
      mr={6}
      display="block"
      as="b"
      _hover={{ color: "teal.800" }}
    >
      {children.nom}
    </Text>
  </Link>
);

export default function Navbar(props) {
  const [show, setShow] = React.useState(false);
  const handleToggle = () => setShow(!show);

  const deconnexion = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("utilisateur");
  };

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
      bg="teal.500"
      color="white"
    >
      <Box width={"17%"}>
        <Flex align="center" mr={5}>
          <Heading
            as="h1"
            fontSize={{ base: "xs", md: "2xl", xl: "3xl" }}
            letterSpacing={"-.1rem"}
          >
            {props.titre}
          </Heading>
        </Flex>
      </Box>

      <Box display={{ base: "block", md: "none" }} onClick={handleToggle}>
        <svg
          fill="white"
          width="12px"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <title>Menu</title>
          <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
        </svg>
      </Box>
      <Box
        display={{ sm: show ? "block" : "none", md: "flex" }}
        width={{ sm: "full", md: "auto" }}
        alignItems="center"
        flexGrow={1}
      >
        {props.onglet.map((value) => {
          return <MenuItems key={value.nom}>{value}</MenuItems>;
        })}
      </Box>
      <Box
        display={{ sm: show ? "block" : "none", md: "block" }}
        mt={{ base: 4, md: 0 }}
      >
        <Flex align="center">
          <FaUserAlt />
          <Text mt={{ base: 4, md: 0 }} mr={6} ml={2} display="block">
            {localStorage.getItem("utilisateur")}
          </Text>
        </Flex>
      </Box>
      <Box
        display={{ sm: show ? "block" : "none", md: "block" }}
        mt={{ base: 4, md: 0 }}
        mr={5}
      >
        {" "}
        <Link to="/">
          <Button
            rightIcon={<BiUserX />}
            bg="transparent"
            border="1px"
            onClick={deconnexion}
          >
            Déconnecter
          </Button>
        </Link>
      </Box>
    </Flex>
  );
}
