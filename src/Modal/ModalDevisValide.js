import React, { useEffect, useState } from "react";
import {
  Box,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  InputGroup,
  InputLeftAddon,
  Input,
  InputRightAddon,
  Text,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModalDevisValide(props) {
  const initListPourcentages = () => {
    const listInit = [];
    props.devis.etapeComposeDevis?.forEach((element) => {
      listInit[element.etape.id - 1] = element.pourcentage;
    });
    return listInit;
  };
  const [listEtapes, setListEtapes] = useState([]);
  const [listPourcentages, setListPourcentages] = useState([]);
  const token = localStorage.getItem("token");
  const utilisateur = localStorage.getItem("utilisateur");
  const handleBlur = (index, event) =>
    UpdateListPourcentages(index, event.target.value);

  const EtapeItems = ({ children }) =>
    children.map((value, index) => {
      return (
        <InputGroup mt={3} width="100%" key={index}>
          <InputLeftAddon children={value.nom} width="75%" />
          <Input
            type="tel"
            placeholder="0"
            width="10%"
            defaultValue={listPourcentages[index]}
            onBlur={(event) => handleBlur(index, event)}
          />
          <InputRightAddon children="%" />
        </InputGroup>
      );
    });

  const UpdateListPourcentages = (index, value) => {
    let newListPourcentage = listPourcentages.slice();
    newListPourcentage[index] = parseInt(value);
    setListPourcentages(newListPourcentage);
  };

  const validerDevis = (event) => {
    event.preventDefault();
    const produit = props.produit;
    produit.validation = true;
    const newDevis = props.devis;
    axios
      .put(`${process.env.REACT_APP_URL_BACKEND}/api/v1/produit`, produit, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        axios
          .get(
            `${process.env.REACT_APP_URL_BACKEND}/api/v1/mails_validation_devis/${utilisateur}/devis/${props.devis.id}/produit/${response.data.id}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then(() => {
            newDevis["etape"] = props.etape;
            axios
              .put(
                `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`,
                newDevis,
                {
                  headers: {
                    Authorization: `Bearer ${token}`,
                  },
                }
              )
              .then(props.reload);
          });
      })
      .then(
        listPourcentages.forEach((element, index) => {
          const etapeDevis = {
            etape: listEtapes[index],
            devis: props.devis,
            pourcentage: element,
          };
          if (element) {
            axios.post(
              `${process.env.REACT_APP_URL_BACKEND}/api/v1/etapeDevis`,
              etapeDevis,
              {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              }
            );
          }
        })
      );
  };

  const totalPourcentage = () => {
    return listPourcentages.reduce((a, b) => a + b, 0);
  };

  useEffect(() => {
    const listEtapes = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/etape`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListEtapes(result.data);
    };
    setListPourcentages(initListPourcentages());
    listEtapes();
  }, [props.isOpen, token]);

  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose} size="xl">
      <ModalOverlay />
      <form onSubmit={validerDevis}>
        <ModalContent>
          <ModalHeader backgroundColor="teal.700" color="white">
            Echelonnements du devis n° {props.devis.numero_devis}
          </ModalHeader>
          <ModalCloseButton backgroundColor="teal.100" />
          <ModalBody>
            <EtapeItems>{listEtapes}</EtapeItems>
            <Box width="25%">
              <Text
                mt={5}
                position="right"
                backgroundColor={
                  totalPourcentage() !== 100 ? "red.100" : "green.100"
                }
              >
                Total :{totalPourcentage()} % <br />
                Total :{(props.produit?.prix_total * totalPourcentage()) /
                  100}{" "}
                €
              </Text>
            </Box>
          </ModalBody>
          <ModalFooter>
            <Button
              type="submit"
              colorScheme="red"
              mr={3}
              onClick={props.onClose}
              disabled={props.dejaValide || totalPourcentage() !== 100}
            >
              Valider
            </Button>
            <Button colorScheme="blue" mr={3} onClick={props.onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  );
}
