import React, { useState } from "react";
import { Box, Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Select,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModalFormDevis(props) {
  const token = localStorage.getItem("token");
  const handleSubmitDevis = (event) => {
    event.preventDefault();
    const dateCreation = new Date();
    const devis = {
      numero_devis:
        "" +
        dateCreation.getFullYear() +
        dateCreation.getDate() +
        dateCreation.getHours() +
        dateCreation.getMinutes(),
      client: props.client,
      etat: "Brouillon",
      adresse: props.client.adresses[adresse],
      produits: [],
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`, devis, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(props.reload)
      .then(props.onClose);
  };

  const [adresse, setAdresse] = useState({});
  const handleChangeAdresse = (event) => setAdresse(event.target.value);

  const AdresseItems = ({ children }) =>
    children.map((value, index) => {
      if (index > 0) {
        return <option value={index}>{value.nom_adr}</option>;
      } else {
        return null;
      }
    });
  return (
    <Box>
      <Modal isOpen={props.isOpen} onClose={props.onClose}>
        <ModalOverlay />
        <form onSubmit={handleSubmitDevis}>
          <ModalContent>
            <ModalHeader backgroundColor="teal.700" color="white">
              Création nouveau devis
            </ModalHeader>
            <ModalCloseButton backgroundColor="teal.100" />
            <ModalBody pb={6}>
              <FormControl mt={4}>
                <FormLabel>Adresse Chantier</FormLabel>
                <Select
                  mt={2}
                  value={adresse}
                  onChange={handleChangeAdresse}
                  placeholder="Choisir Adresse"
                  isRequired={true}
                >
                  <AdresseItems>{props.client?.adresses}</AdresseItems>
                </Select>
              </FormControl>
            </ModalBody>

            <ModalFooter>
              <Button type="submit" colorScheme="blue" mr={3}>
                Enregistrer
              </Button>
              <Button onClick={props.onClose}>Quitter</Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </Box>
  );
}
