import React, { useState } from "react";
import { Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  RadioGroup,
  Stack,
  Radio,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModalChoixMaison(props) {
  const token = localStorage.getItem("token");
  let tansmissionTokenApi = () => {
    var windowReference = window.open();
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/token`, token, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        windowReference.location = `${process.env.REACT_APP_URL_EDITOR}/editor/?produitId=${props.produit.id}&maisonType=${choixSelect}&gamme=${props.produit.gamme.nom}&keyToken=${result.data}`;
      })
      .then(props.onClose);
  };
  const [choixSelect, setChoixSelect] = useState("vide");
  const handleChoixSelect = (event) => {
    setChoixSelect(event.target.value);
  };

  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader backgroundColor="teal.700" color="white">
          Initialisation du produit
        </ModalHeader>
        <ModalCloseButton backgroundColor="teal.100" />
        <ModalBody pb={6}>
          <FormControl mt={4}>
            <FormLabel>Choix d'Initialisation</FormLabel>

            <RadioGroup defaultValue={choixSelect} onClick={handleChoixSelect}>
              <Stack>
                <Radio value="vide">Nouveau plan vierge</Radio>
                <Radio value={"type_" + props.produit?.gamme?.nom}>
                  A partir d'une maison type : {props.produit?.gamme?.nom}
                </Radio>
              </Stack>
            </RadioGroup>
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={tansmissionTokenApi}>
            Valider
          </Button>
          <Button onClick={props.onClose}>Quitter</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
