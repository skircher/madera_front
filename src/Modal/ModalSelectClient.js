import React, { useState, useEffect } from "react";
import { Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Select,
  useDisclosure,
} from "@chakra-ui/react";
import ModalFormDevis from "../Modal/ModalFormDevis";
import axios from "axios";

export default function ModalSelectClient(props) {
  const token = localStorage.getItem("token");
  const ClientItems = ({ children }) =>
    children.map((value, index) => {
      return <option value={index}>{`${value.nom} ${value.prenom}`}</option>;
    });

  const [listClient, setListClient] = useState([]);
  const {
    isOpen: isOpenSelectAdresse,
    onOpen: onOpenSelectAdresse,
    onClose: onCloseSelectAdresse,
  } = useDisclosure();

  const [clientSelect, setClientSelect] = useState({});
  const handleClientSelect = (event) => {
    setClientSelect(listClient[event.target.value]);
  };

  const closeModal = () => {
    document.location.reload();
  };

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_URL_BACKEND}/api/v1/client`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setListClient(result.data);
      });
  }, [token]);
  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader backgroundColor="teal.700" color="white">
          Choix du client
        </ModalHeader>
        <ModalCloseButton backgroundColor="teal.100" />
        <ModalBody pb={6}>
          <FormControl mt={4}>
            <FormLabel>Client</FormLabel>
            <Select
              mt={2}
              value={listClient.indexOf(clientSelect)}
              onChange={handleClientSelect}
              placeholder="Choisir Client"
              isRequired={true}
            >
              <ClientItems>{listClient}</ClientItems>
            </Select>
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={onOpenSelectAdresse}>
            Enregistrer
          </Button>
          <ModalFormDevis
            isOpen={isOpenSelectAdresse}
            onClose={onCloseSelectAdresse}
            client={clientSelect}
            closePrevModal={props.onClose}
            reload={closeModal}
          ></ModalFormDevis>
          <Button onClick={props.onClose}>Quitter</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
