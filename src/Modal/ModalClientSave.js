import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";

export default function ModalClientSave(props) {
  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader backgroundColor="teal.700" color="white">
          Sauvegarde Client
        </ModalHeader>
        <ModalCloseButton backgroundColor="teal.100" />
        <ModalBody>Le client {props.client.nom} est bien Sauvegardé</ModalBody>
        <ModalFooter>
          <Link to={{ pathname: `/FicheClient/${props.client.id}` }}>
            <Button colorScheme="blue" mr={3}>
              Close
            </Button>
          </Link>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
