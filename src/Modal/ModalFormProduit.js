import React, { useEffect, useState } from "react";
import { Box, Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Select,
  Input,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModalFormProduit(props) {
  const token = localStorage.getItem("token");
  const [gammes, setGammes] = useState([]);
  const [choixGamme, setChoixGamme] = useState("");
  const [nomProduit, setNomProduit] = useState("");
  const handleChangeNomProduit = (event) => setNomProduit(event.target.value);

  const handleChangeGamme = (event) => setChoixGamme(event.target.value);

  const handleSubmitDevis = (event) => {
    event.preventDefault();
    console.log(choixGamme);
    const produit = {
      nom: nomProduit,
      gamme: gammes[choixGamme],
      devis: props.devis,
      validation: false,
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/produit`, produit, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(props.reload)
      .then(props.onClose);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/gamme`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setGammes(result.data);
    };
    fetchData();
  }, [token]);

  const GammeItems = ({ children }) =>
    children.map((value, index) => {
      return (
        <option value={index} key={index}>
          {value.nom}
        </option>
      );
    });

  return (
    <Box>
      <Modal isOpen={props.isOpen} onClose={props.onClose}>
        <ModalOverlay />
        <form onSubmit={handleSubmitDevis}>
          <ModalContent>
            <ModalHeader backgroundColor="teal.700" color="white">
              Création nouveau produit
            </ModalHeader>
            <ModalCloseButton backgroundColor="teal.100" />
            <ModalBody pb={6}>
              <FormControl>
                <FormLabel>Nom du produit</FormLabel>
                <Input
                  value={nomProduit}
                  onChange={handleChangeNomProduit}
                  placeholder="Nom du produit"
                  isRequired={true}
                />
              </FormControl>
              <FormControl mt={4}>
                <FormLabel>Choix de la gamme</FormLabel>
                <Select
                  mt={2}
                  value={choixGamme}
                  onChange={handleChangeGamme}
                  placeholder="Choisir une gamme"
                  isRequired={true}
                >
                  <GammeItems>{gammes}</GammeItems>
                </Select>
              </FormControl>
            </ModalBody>

            <ModalFooter>
              <Button type="submit" colorScheme="blue" mr={3}>
                Enregistrer
              </Button>
              <Button onClick={props.onClose}>Quitter</Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </Box>
  );
}
