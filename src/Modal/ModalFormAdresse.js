import React, { useState, useEffect } from "react";
import { Box, Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
  Select,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModaleFormAdresse(props) {
  const token = localStorage.getItem("token");
  const VilleItems = ({ children }) =>
    children.map((value, index) => {
      return <option value={index}>{value.nom}</option>;
    });

  const [nomAdresse, setNomAdresse] = useState("");
  const handleChangeNomAdresse = (event) => setNomAdresse(event.target.value);

  const [rue, setRue] = useState("");
  const handleChangeRue = (event) => setRue(event.target.value);

  const [complement, setComplement] = useState("");
  const handleChangeComplement = (event) => setComplement(event.target.value);

  const [numero, setNumero] = useState(10);

  const [ville, setVille] = useState(null);
  const handleChangeVille = (event) => setVille(event.target.value);

  const [listeVille, setListeVille] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/ville/`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListeVille(result.data);
    };
    fetchData();
  }, [token]);

  const handleSubmitAdresse = (event) => {
    event.preventDefault();
    const adresse = {
      nom_adr: nomAdresse,
      rue: rue,
      numero: parseInt(numero),
      complement: complement,
      ville: listeVille[ville],
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/adresse`, adresse, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        axios
          .post(
            `${process.env.REACT_APP_URL_BACKEND}/api/v1/client/adresse/${props.clientId}`,
            result.data,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then(props.reload)
          .then(props.onClose);
      });
  };
  return (
    <Box>
      <Modal isOpen={props.isOpen} onClose={props.onClose}>
        <ModalOverlay />
        <form onSubmit={handleSubmitAdresse}>
          <ModalContent>
            <ModalHeader backgroundColor="teal.700" color="white">
              Ajout nouvelle adresse chantier
            </ModalHeader>
            <ModalCloseButton backgroundColor="teal.100" />
            <ModalBody pb={6}>
              <FormControl mt={4}>
                <FormLabel>Nom de l'adresse</FormLabel>
                <Input
                  mt={2}
                  value={nomAdresse}
                  onChange={handleChangeNomAdresse}
                  placeholder="Nom adresse"
                  isRequired={true}
                />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Adresse</FormLabel>
                <Input
                  mt={2}
                  value={rue}
                  onChange={handleChangeRue}
                  placeholder="Rue"
                  isRequired={true}
                />
                <Input
                  mt={2}
                  value={complement}
                  onChange={handleChangeComplement}
                  placeholder="Complément"
                />
                <NumberInput
                  onChange={(valueString) => setNumero(valueString)}
                  value={numero}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Select
                  mt={2}
                  value={ville}
                  onChange={handleChangeVille}
                  placeholder="Choisir ville"
                  isRequired={true}
                >
                  <VilleItems>{listeVille}</VilleItems>
                </Select>
              </FormControl>
            </ModalBody>

            <ModalFooter>
              <Button type="submit" colorScheme="blue" mr={3}>
                Enregistrer
              </Button>
              <Button onClick={props.onClose}>Quitter</Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </Box>
  );
}
