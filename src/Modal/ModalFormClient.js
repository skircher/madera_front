import React, { useState, useEffect } from "react";
import { Box, Button } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
  Radio,
  RadioGroup,
  Stack,
  Select,
  useDisclosure,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";
import axios from "axios";

import ModalClientSave from "./ModalClientSave";

export default function ModalFormClient(props) {
  const token = localStorage.getItem("token");

  const VilleItems = ({ children }) =>
    children.map((value, index) => {
      return <option value={index}>{value.nom}</option>;
    });

  const handleSubmitAdresse = (event) => {
    event.preventDefault();
    const adresse = {
      nom_adr: "Adresse personnelle",
      rue: rue,
      numero: parseInt(numero),
      complement: complement,
      ville: listeVille[ville],
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/adresse`, adresse, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        handleSubmitClient(result.data);
      });
  };

  const handleSubmitClient = (adrClient) => {
    const client = {
      prenom: prenomClient,
      nom: nomClient,
      email: email,
      telephone_fixe: telFix,
      telephone_port: telPort,
      genre: genre === "0" ? false : true,
      adresses: [adrClient],
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/client`, client, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res);
        console.log(res.data);
        setNouveauClient(res.data);
        onOpen();
      });
  };

  const [nouveauClient, setNouveauClient] = useState("");

  const [prenomClient, setPrenomClient] = useState("");
  const handleChangePrenom = (event) => setPrenomClient(event.target.value);

  const [nomClient, setNomClient] = useState("");
  const handleChangeNom = (event) => setNomClient(event.target.value);

  const [email, setEmail] = useState("");
  const handleChangeEmail = (event) => setEmail(event.target.value);

  const [telFix, setTelFix] = useState("");
  const handleChangeTelFix = (event) => setTelFix(event.target.value);

  const [telPort, setTelPort] = useState("");
  const handleChangeTelPort = (event) => setTelPort(event.target.value);

  const [genre, setGenre] = useState("0");

  const [rue, setRue] = useState("");
  const handleChangeRue = (event) => setRue(event.target.value);

  const [complement, setComplement] = useState("");
  const handleChangeComplement = (event) => setComplement(event.target.value);

  const [numero, setNumero] = useState(10);

  const [ville, setVille] = useState(null);
  const handleChangeVille = (event) => setVille(event.target.value);

  const [listeVille, setListeVille] = useState([]);

  const { isOpen, onOpen } = useDisclosure();

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/ville/`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListeVille(result.data);
    };
    fetchData();
  }, [token]);
  return (
    <Box>
      <Modal isOpen={props.isOpen} onClose={props.onClose}>
        <ModalOverlay />
        <form onSubmit={handleSubmitAdresse}>
          <ModalContent>
            <ModalHeader backgroundColor="teal.700" color="white">
              Création fiche client
            </ModalHeader>
            <ModalCloseButton backgroundColor="teal.100" />
            <ModalBody pb={6}>
              <FormControl>
                <FormLabel>Nom</FormLabel>
                <Input
                  value={nomClient}
                  onChange={handleChangeNom}
                  placeholder="Nom"
                  isRequired={true}
                />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Prénom</FormLabel>
                <Input
                  value={prenomClient}
                  onChange={handleChangePrenom}
                  placeholder="Prénom"
                  isRequired={true}
                />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Genre</FormLabel>
                <RadioGroup onChange={(e) => setGenre(e)} value={genre}>
                  <Stack direction="row">
                    <Radio value="0">Femme</Radio>
                    <Radio value="1">Homme</Radio>
                  </Stack>
                </RadioGroup>
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Adresse</FormLabel>
                <Input
                  mt={2}
                  value={rue}
                  onChange={handleChangeRue}
                  placeholder="Rue"
                  isRequired={true}
                />
                <Input
                  mt={2}
                  value={complement}
                  onChange={handleChangeComplement}
                  placeholder="Complément"
                />
                <NumberInput
                  onChange={(valueString) => setNumero(valueString)}
                  value={numero}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Select
                  mt={2}
                  value={ville}
                  onChange={handleChangeVille}
                  isRequired={true}
                  placeholder="Choisir ville"
                >
                  <VilleItems>{listeVille}</VilleItems>
                </Select>
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Adresse Mail</FormLabel>
                <Input
                  value={email}
                  onChange={handleChangeEmail}
                  type="email"
                  placeholder="Adresse mail"
                  isRequired={true}
                />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Téléphone(fixe)</FormLabel>
                <Input
                  value={telFix}
                  onChange={handleChangeTelFix}
                  placeholder="Téléphone(fixe)"
                  isRequired={true}
                />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>Téléphone(portable)</FormLabel>
                <Input
                  value={telPort}
                  onChange={handleChangeTelPort}
                  placeholder="Téléphone(portable)"
                  isRequired={true}
                />
              </FormControl>
            </ModalBody>

            <ModalFooter>
              <Button type="submit" colorScheme="blue" mr={3}>
                Enregistrer
                <ModalClientSave
                  onClose={props.onClose}
                  isOpen={isOpen}
                  client={nouveauClient}
                />
              </Button>
              <Button onClick={props.onClose}>Quitter</Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </Box>
  );
}
