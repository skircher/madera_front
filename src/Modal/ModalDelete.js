import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
} from "@chakra-ui/react";
import axios from "axios";

export default function ModalDelete(props) {
  const token = localStorage.getItem("token");
  const handleSubmitDelete = (event) => {
    event.preventDefault();
    axios
      .delete(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/produit/${props.id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(props.reload);
  };
  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <ModalOverlay />
      <form onSubmit={handleSubmitDelete}>
        <ModalContent>
          <ModalHeader backgroundColor="teal.700" color="white">
            Suppression {props.elementModel}
          </ModalHeader>
          <ModalCloseButton backgroundColor="teal.100" />
          <ModalBody>
            Voulez-vous supprimer le {props.elementModel} "{props.nom}" ?
          </ModalBody>
          <ModalFooter>
            <Button
              type="submit"
              colorScheme="red"
              mr={3}
              onClick={props.onClose}
            >
              Supprimer
            </Button>
            <Button mr={3} onClick={props.onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  );
}
