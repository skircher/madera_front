import React, { useEffect, useState } from "react";
import {
  Box,
  Text,
  SimpleGrid,
  Grid,
  GridItem,
  Heading,
  Button,
  useDisclosure,
} from "@chakra-ui/react";
import Navbar from "../Composant/Navbar";
import axios from "axios";
import ListeElement from "../Composant/ListeElement";
import { BsPlusSquare } from "react-icons/bs";
import { useHistory } from "react-router-dom";
import ModalFormAdresse from "../Modal/ModalFormAdresse";
import ModalFormDevis from "../Modal/ModalFormDevis";

export default function FicheClient(props) {
  const colonneNoms = ["Numéro", "Nom Adresse", "Etat", "Etape", "Mise à jour"];
  const [clientDevis, setClientDevis] = useState([]);
  const [listDevis, setListDevis] = useState([]);
  const [majPage, setMajPage] = useState();
  const history = useHistory();
  const token = localStorage.getItem("token");

  const onglet = [
    { nom: "Accueil", url: "/Accueil" },
    {
      nom: "Détail Client",
      url: `/FicheClient/${clientDevis.id}`,
    },
  ];

  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isOpenDevis,
    onOpen: onOpenDevis,
    onClose: onCloseDevis,
  } = useDisclosure();

  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }
    const fetchDataClient = async () => {
      await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/client/${props.match.params.idClient}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      ).then((response) => {
        setClientDevis(response.data);
      });
    };
    fetchDataClient();
    const fetchDataClientDevis = async () => {
      await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis/client/${props.match.params.idClient}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      ).then((response) => {
        setListDevis(response.data);
      });
    };
    fetchDataClientDevis();
    console.log("test");
  }, [props.match.params.idClient, majPage, history, token]);

  let miseEnPageTableauDevis = () => {
    let listDevisFormat = [];
    listDevis.forEach((devis) => {
      const detailDevis = {
        get_url: `/DetailDevis/${devis.id}`,
        state: token,
        Num_devis: devis.numero_devis,
        Adresse: devis.adresse.nom_adr,
        Etat: devis.etat,
        Etapes: "",
        date_maj: devis.date_maj.split("T")[0],
      };
      listDevisFormat.push(detailDevis);
    });
    return listDevisFormat;
  };

  return (
    <Box>
      <Navbar titre={onglet[1].nom} onglet={onglet} />
      <Grid gap={2}>
        <GridItem
          colStart={0}
          colEnd={1}
          style={{ maxHeight: "85vh", overflow: "auto" }}
        >
          <SimpleGrid
            columns={2}
            spacing={2}
            mt={5}
            ml={10}
            borderWidth="1px"
            borderRadius="lg"
            width={{ base: "100%", md: "90%", xl: "75%" }}
            padding={5}
            backgroundColor="teal.100"
            color="teal.500"
            fontSize={{ base: "md", md: "lg", xl: "xl" }}
          >
            <Text as="b">Nom : </Text>
            <Text as="i" color="grey">
              {clientDevis.nom?.toUpperCase()}
            </Text>
            <Text as="b">Prénom : </Text>
            <Text as="i" color="grey">
              {clientDevis.prenom}
            </Text>
            <Text as="b">Adresse : </Text>
            <Box>
              <Box>
                <Text as="i" color="grey">
                  {clientDevis.adresses ? clientDevis.adresses[0].nom_adr : ""}
                </Text>
              </Box>
              <Box>
                <Text as="i" color="grey">
                  {clientDevis.adresses ? clientDevis.adresses[0].rue : ""}
                </Text>
              </Box>
              <Box>
                <Text as="i" color="grey">
                  {clientDevis.adresses ? clientDevis.adresses[0].numero : ""}
                </Text>
                <Text as="i" color="grey">
                  {" "}
                  {clientDevis.adresses
                    ? clientDevis.adresses[0].complement
                    : ""}
                </Text>
              </Box>
              <Box>
                <Text as="i" color="grey">
                  {clientDevis.adresses
                    ? clientDevis.adresses[0].ville.code_postal
                    : ""}
                </Text>
                <Text as="i" color="grey">
                  {" "}
                  {clientDevis.adresses
                    ? clientDevis.adresses[0].ville.nom
                    : ""}
                </Text>
              </Box>
            </Box>
            <Text as="b">Adresse Mail : </Text>
            <Text as="i" color="grey">
              {clientDevis.email}
            </Text>
            <Text as="b">Téléphone : </Text>
            <Text as="i" color="grey">
              {clientDevis.telephone_fixe} <br /> {clientDevis.telephone_port}
            </Text>
            <Text as="b">Genre : </Text>
            <Text as="i" color="grey">
              {clientDevis.genre ? "Masculin" : "Féminin"}
            </Text>
            <Text as="b">Crée le : </Text>
            <Text as="i" color="grey">
              {clientDevis.date_creation
                ? clientDevis.date_creation.split("T")[0]
                : clientDevis.date_creation}
            </Text>
            <Text as="b">
              Adresse(s) Chantier(s) :{" "}
              <Button
                bg="transparent"
                border="1px"
                leftIcon={<BsPlusSquare />}
                onClick={onOpen}
              >
                Ajouter
              </Button>
            </Text>
            <Box>
              {clientDevis.adresses
                ? clientDevis.adresses.map((value, index) => {
                    if (index > 0) {
                      return (
                        <Box mb={2}>
                          <Box>
                            <Text as="b" color="grey">
                              {value.nom_adr}
                            </Text>
                          </Box>
                          <Box>
                            <Text as="i" color="grey">
                              {value.rue}
                            </Text>
                          </Box>
                          <Box>
                            <Text as="i" color="grey">
                              {value.numero}
                            </Text>
                            <Text as="i" color="grey">
                              {" "}
                              {value.complement}
                            </Text>
                          </Box>
                          <Box>
                            <Text as="i" color="grey">
                              {value.ville.code_postal}
                            </Text>
                            <Text as="i" color="grey">
                              {" "}
                              {value.ville.nom}
                            </Text>
                          </Box>
                        </Box>
                      );
                    } else {
                      return null;
                    }
                  })
                : null}
            </Box>
          </SimpleGrid>
          <ModalFormAdresse
            isOpen={isOpen}
            onClose={onClose}
            clientId={clientDevis.id}
            reload={setMajPage}
          />
        </GridItem>
        <GridItem colStart={1} colEnd={2}>
          <Heading
            fontSize={{ base: "md", md: "2xl", xl: "4xl" }}
            mt={5}
            mb={5}
          >
            Liste des devis client
          </Heading>
          <ModalFormDevis
            isOpen={isOpenDevis}
            onClose={onCloseDevis}
            client={clientDevis}
            reload={setMajPage}
          />
          <ListeElement
            nomColonne={colonneNoms}
            data={miseEnPageTableauDevis()}
            dessousListe={listDevis.length > 0 ? "" : "Aucun devis en cours"}
          />
          <Button
            bg="transparent"
            border="1px"
            width="25"
            margin="2"
            leftIcon={<BsPlusSquare />}
            onClick={onOpenDevis}
          >
            Ajouter
          </Button>
        </GridItem>
      </Grid>
    </Box>
  );
}
