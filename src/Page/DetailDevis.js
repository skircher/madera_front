import React, { useEffect, useState } from "react";
import {
  Box,
  Heading,
  SimpleGrid,
  Text,
  Select,
  Button,
  Grid,
  GridItem,
  useDisclosure,
} from "@chakra-ui/react";
import Navbar from "../Composant/Navbar";
import BlocProduit from "../Composant/BlocProduit";
import ModalFormProduit from "../Modal/ModalFormProduit";
import ModalDevisValide from "../Modal/ModalDevisValide";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function DetailDevis(props) {
  const maisonImage = {
    Ecologique: "/images/gamme_ecologique.png",
    Hivernale: "/images/gamme_hivernale.jpg",
    Economique: "/images/gamme_economique.png",
  };

  const history = useHistory();
  const token = localStorage.getItem("token");

  const ProduitItems = ({ children }) =>
    children.map((value) => {
      return (
        <BlocProduit
          id={value.id}
          image={maisonImage[value.gamme?.nom]}
          nom={value.nom}
          gamme={value.gamme?.nom}
          prix={value.prix_total}
          cree={value?.date_creation.split("T")[0]}
          maj={value?.date_maj.split("T")[0]}
          devis={props.match.params.idDevis}
          reload={setMajPage}
          validation={value.validation ? "green.500" : "white"}
        />
      );
    });

  const [devisSelect, setDevisSelect] = useState([]);
  const [clientDevis, setClientDevis] = useState([]);
  const [adresse, setAdresse] = useState([]);
  const [ville, setVille] = useState([]);
  const [produitDevis, setProduitDevis] = useState([]);
  const [majPage, setMajPage] = useState();
  const [produitValide, setProduitValide] = useState(null);
  const [listeEtapes, setListeEtapes] = useState([]);
  const handleChangeProduitValide = (event) => {
    console.log(produitDevis[event.target.value]);
    setProduitValide(produitDevis[event.target.value]);
  };

  const handleChangeEtatDevis = (event) => {
    const newDevis = devisSelect;
    newDevis["etat"] = event.target.value;
    axios
      .put(`${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`, newDevis, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(setMajPage);
  };

  const handleChangeEtapeDevis = (event) => {
    const newDevis = devisSelect;
    newDevis["etape"] = listeEtapes[event.target.value];
    axios
      .put(`${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`, newDevis, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(setMajPage);
  };

  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isOpenDevisValide,
    onOpen: onOpenDevisValide,
    onClose: onCloseDevisValide,
  } = useDisclosure();
  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }
    const fetchData = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis/${props.match.params.idDevis}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setDevisSelect(result.data);
      setClientDevis(result.data.client);
      setAdresse(result.data.adresse);
      setVille(result.data.adresse.ville);
      setProduitDevis(result.data.produits);
    };
    fetchData();

    axios
      .get(`${process.env.REACT_APP_URL_BACKEND}/api/v1/etape`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        setListeEtapes(result.data);
      });
  }, [
    props.match.params.idClient,
    majPage,
    token,
    props.match.params.idDevis,
    history,
  ]);

  let onglet = [
    { nom: "Accueil", url: "/Accueil" },
    {
      nom: "Détail Client",
      url: `/FicheClient/${clientDevis.id}`,
    },
  ];

  const etats = [
    "Brouillon",
    "Attente_client",
    "En_commande",
    "En_facturation",
  ];

  const EtatSelect = ({ children }) =>
    children.map((value) => {
      return <option value={value}>{value.replace("_", " ")}</option>;
    });

  return (
    <Box height="100%" display="flex" flexDirection="column">
      <Navbar titre={"Détail Devis"} onglet={onglet} />
      <Grid gap={2}>
        <GridItem
          colStart={0}
          colEnd={1}
          style={{ maxHeight: "85vh", overflow: "auto" }}
        >
          <Box ml={{ base: 2, md: 2, xl: 5 }}>
            <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }}>
              Informations
            </Heading>
            <SimpleGrid
              columns={2}
              spacing={2}
              mt={2}
              ml={{ base: 2, md: 5, xl: 10 }}
              borderWidth="1px"
              borderRadius="lg"
              padding={2}
              backgroundColor="teal.100"
              color="teal.500"
              fontSize={{ base: "md", md: "lg", xl: "xl" }}
            >
              <Text as="b">Devis N° : </Text>
              <Text as="i" color="grey">
                {devisSelect.numero_devis}
              </Text>
              <Text as="b">Nom : </Text>
              <Text as="i" color="grey">
                {clientDevis.nom}
              </Text>
              <Text as="b">Prénom : </Text>
              <Text as="i" color="grey">
                {clientDevis.prenom}
              </Text>
              <Text as="b">Adresse Chantier : </Text>
              <SimpleGrid>
                <Text as="i" color="grey">
                  {adresse.nom_adr}
                </Text>
                <Text as="i" color="grey">
                  {adresse.numero}
                </Text>
                <Text as="i" color="grey">
                  {adresse.rue}
                </Text>
                <Text as="i" color="grey">
                  {ville.nom}
                </Text>
                <Text as="i" color="grey">
                  {ville.code_postal}
                </Text>
              </SimpleGrid>
              <Text as="b">Créé le : </Text>
              <Text as="i" color="grey">
                {devisSelect.date_creation
                  ? devisSelect.date_creation.split("T")[0]
                  : devisSelect.date_creation}
              </Text>
              <Text as="b">Etat devis : </Text>
              <Select
                value={devisSelect.etat}
                onChange={handleChangeEtatDevis}
                w="90%"
                backgroundColor="teal.200"
                color="black"
              >
                <EtatSelect>{etats}</EtatSelect>
              </Select>
              <Text as="b">Etape devis : </Text>
              <Select
                value={devisSelect.etape?.id - 1}
                onChange={handleChangeEtapeDevis}
                w="90%"
                backgroundColor="teal.200"
                color="black"
                placeholder="Aucune étape en cours"
              >
                {listeEtapes.map((value, index) => {
                  return <option value={index}>{value.nom}</option>;
                })}
              </Select>
            </SimpleGrid>
          </Box>
          <Box mt={{ base: 1, md: 2, xl: 2 }} ml={5}>
            <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }}>
              Actions
            </Heading>
            <SimpleGrid columns={2} spacing={5} mt={5} ml={5} fontSize="2xl">
              <Text fontSize={{ base: "lg", md: "lg", xl: "xl" }}>
                Sélectionner Produit :{" "}
              </Text>
              <Select
                value={produitDevis.indexOf(produitValide)}
                onChange={handleChangeProduitValide}
                placeholder="Select produit"
                w="75%"
              >
                {produitDevis.map((prod, index) => {
                  return <option value={index}>{prod.nom}</option>;
                })}
              </Select>
              <Text fontSize={{ base: "lg", md: "lg", xl: "xl" }}>
                Echelonner Paiements :
              </Text>
              <Button
                onClick={onOpenDevisValide}
                w="75%"
                disabled={
                  produitDevis.indexOf(produitValide) !== -1 ? false : true
                }
              >
                Editer/Visualiser
              </Button>
              <ModalDevisValide
                isOpen={isOpenDevisValide}
                onClose={onCloseDevisValide}
                reload={setMajPage}
                produit={produitValide}
                devis={devisSelect}
                etape={listeEtapes[0]}
                dejaValide={
                  produitDevis !== undefined
                    ? produitDevis.some((produit) => produit.validation)
                    : false
                }
              />
            </SimpleGrid>
          </Box>
        </GridItem>
        <GridItem colStart={1} colEnd={2}>
          <Box>
            <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }}>
              Produits
            </Heading>
            <SimpleGrid
              columns={{ base: 1, md: 3, xl: 4 }}
              spacing={5}
              mt={5}
              ml={{ base: 2, md: 5, xl: 10 }}
              mr={{ base: 2, md: 2, xl: 5 }}
              style={{ maxHeight: "75vh", overflow: "auto" }}
            >
              <ProduitItems>{produitDevis}</ProduitItems>
              <Button
                bg="transparent"
                marginLeft="2"
                border="1px"
                width="5%"
                margin="2"
                fontSize="2xl"
                onClick={onOpen}
                disabled={
                  produitDevis !== undefined
                    ? produitDevis.some((produit) => produit.validation)
                    : false
                }
              >
                +
              </Button>
              <ModalFormProduit
                isOpen={isOpen}
                onClose={onClose}
                reload={setMajPage}
                devis={devisSelect}
              />
            </SimpleGrid>
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
}
