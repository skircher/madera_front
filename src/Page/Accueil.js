import React, { useEffect, useState } from "react";
import {
  Box,
  Grid,
  GridItem,
  Heading,
  Button,
  Flex,
  useDisclosure,
  Divider,
} from "@chakra-ui/react";
import Navbar from "../Composant/Navbar";
import ListeElement from "../Composant/ListeElement";
import ModalFormClient from "../Modal/ModalFormClient";
import ModalSelectClient from "../Modal/ModalSelectClient";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function Accueil(props) {
  const onglet = [
    { nom: "Accueil", url: "/Accueil" },
    { nom: "Liste Clients", url: "/ListeClient" },
    { nom: "Liste Devis", url: "/ListeDevis" },
    { nom: "Stats", url: "/" },
  ];
  const colonneNoms = ["N°Devis", "Clients", "Etat", "Etape", "Mise à jour"];
  const [listDevis, setListDevis] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isOpenDevis,
    onOpen: onOpenDevis,
    onClose: onCloseDevis,
  } = useDisclosure();
  const {
    isOpen: isOpenSelectAdresse,
    onOpen: onOpenSelectAdresse,
    onClose: onCloseSelectAdresse,
  } = useDisclosure();
  const history = useHistory();
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }

    const fetchData = async () => {
      const result = await axios.get(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListDevis(
        result.data.sort(function (a, b) {
          return new Date(b.date_maj) - new Date(a.date_maj);
        })
      );
    };
    fetchData();
  }, [history, token]);

  let formatDevis = () => {
    let listDevisFormat = [];
    listDevis.forEach((devis) => {
      let newDevis = {
        get_url: "/DetailDevis/" + devis.id,
        state: props.location.state,
        num_devis: devis.numero_devis,
        nom_client: devis.client.prenom + " " + devis.client.nom,
        etat_devis: devis.etat,
        etape_devis: devis.etape?.nom,
        date_maj: devis.date_maj.split("T")[0],
      };
      listDevisFormat.push(newDevis);
    });
    return listDevisFormat;
  };
  return (
    <Box height="100%" display="flex" flexDirection="column">
      <Navbar titre={onglet[0].nom} onglet={onglet} />
      <Grid templateColumns="repeat(2, 1fr)" gap={4}>
        <GridItem colStart={0} colEnd={2} h="100%" align="center">
          <Box
            w={{
              base: "75%", // 0-48em
              md: "80%", // 48em-80em,
              xl: "80%", // 80em+
            }}
            h={{ base: "80%", md: "30%", xl: "100%" }}
            bg="teal.500"
            borderRadius="xl"
            color="white"
            border="2px"
            borderColor="gray.200"
            mt={{ base: "12%", md: "16%", xl: "12%" }}
          >
            <Flex align="center">
              <Box mt={10} ml={10}>
                <Heading fontSize={{ base: "l", md: "xl", xl: "4xl" }}>
                  Ajout nouveau client :
                </Heading>
              </Box>
              <Box mt={10} ml={10}>
                <Button bg="transparent" border="1px" onClick={onOpen}>
                  Valider
                </Button>
              </Box>
            </Flex>
            <ModalFormClient isOpen={isOpen} onClose={onClose} />
            <Flex align="center">
              <Box mt={10} ml={10}>
                <Heading fontSize={{ base: "l", md: "xl", xl: "4xl" }}>
                  Création devis :
                </Heading>
              </Box>
              <Button
                mt={10}
                ml={10}
                bg="transparent"
                border="1px"
                onClick={onOpenDevis}
              >
                Valider
              </Button>
              <ModalSelectClient
                isOpen={isOpenDevis}
                onClose={onCloseDevis}
              ></ModalSelectClient>
            </Flex>
          </Box>
        </GridItem>
        <GridItem colStart={2} colEnd={2} h="10">
          <Heading mt={5} mb={5}>
            Derniers Evenements
          </Heading>

          <Divider />
          <ListeElement
            nomColonne={colonneNoms}
            data={formatDevis()}
            dessousListe="Liste des derniers évènements clients"
          />
        </GridItem>
      </Grid>
    </Box>
  );
}
