import React, { useState, useEffect } from "react";
import {
  Box,
  Heading,
  Input,
  Stack,
  Center,
  Button,
  Image,
  Text,
} from "@chakra-ui/react";
import { useHistory } from "react-router-dom";
import { AiOutlineArrowRight } from "react-icons/ai";
import axios from "axios";

export default function PageAuthentification() {
  const handleSubmit = (event) => {
    event.preventDefault();
    const user = {
      username: utilisateur,
      password: password,
    };
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/user/login`, user)
      .then((result) => {
        setUtilisateur(result.data.split(" ")[0]);
        setStatus(result.status);
        localStorage.setItem("token", result.data.split(" ")[1]);
        localStorage.setItem("utilisateur", result.data.split(" ")[0]);
      })
      .catch(function (error) {
        if (error.response) {
          setStatus(error.response.status);
          setErrorAuth(true);
        }
      });
  };
  const [utilisateur, setUtilisateur] = useState("");
  const handleChangeUtilisateur = (event) => setUtilisateur(event.target.value);

  const [password, setPassword] = useState("");
  const handleChangePassword = (event) => setPassword(event.target.value);

  const [status, setStatus] = useState(403);

  const [errorAuth, setErrorAuth] = useState(false);

  const history = useHistory();

  useEffect(() => {
    handleNextPage(status);
  }, [status]);

  function handleNextPage(status) {
    if (status !== 200) {
      history.push("/");
    } else {
      history.push("/Accueil");
    }
  }
  return (
    <Center h="600px" marginTop="7%">
      <Box
        w="50%"
        h="75%"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        backgroundColor="teal.200"
      >
        <Stack spacing={3}>
          <Center>
            <Image boxSize="150px" src="./logo_madera.png" alt="logo_madera" />
            <Heading
              as="h1"
              fontSize={{ base: "xl", md: "3xl", lg: "4xl" }}
              isTruncated
              alignItems="center"
            >
              Authentification
            </Heading>
          </Center>
          <form onSubmit={handleSubmit}>
            <Center p="3">
              <Input
                placeholder="Utilisateur"
                size="md"
                w="50%"
                isRequired={true}
                onChange={handleChangeUtilisateur}
                backgroundColor="white"
              />
            </Center>
            <Center p="3">
              <Input
                placeholder="Mot de passe"
                size="md"
                w="50%"
                isRequired={true}
                onChange={handleChangePassword}
                backgroundColor="white"
                type={false ? "text" : "password"}
              />
            </Center>
            <Center p="3">
              <Button
                type="submit"
                rightIcon={<AiOutlineArrowRight />}
                colorScheme="teal"
              >
                Connexion
              </Button>
            </Center>
          </form>
          <Center>
            {errorAuth ? (
              <Box>
                <Text color="red.500" as="b">
                  Le mot de passe et/ou utilisateur entré est incorrect. !
                </Text>
              </Box>
            ) : (
              <Text></Text>
            )}
          </Center>
        </Stack>
      </Box>
    </Center>
  );
}
