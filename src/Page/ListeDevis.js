import React, { useEffect, useState } from "react";
import {
  Box,
  SimpleGrid,
  Grid,
  GridItem,
  Input,
  Text,
  Select,
} from "@chakra-ui/react";
import Navbar from "../Composant/Navbar";
import BlocDevis from "../Composant/BlocDevis";
import { useHistory } from "react-router-dom";
import axios from "axios";

const etatDevis = {
  Brouillon: "/images/brouillon.png",
  Attente_client: "/images/attente_client.png",
  En_commande: "/images/en_commande.png",
  En_facturation: "/images/en_facturation.png",
};

const etats = ["Brouillon", "Attente_client", "En_commande", "En_facturation"];

const EtatSelect = ({ children }) =>
  children.map((value) => {
    return <option value={value}>{value.replace("_", " ")}</option>;
  });

const EtapeSelect = ({ children }) =>
  children.map((value) => {
    return <option value={value.nom}>{value.nom}</option>;
  });

export default function ListeDevis(props) {
  const onglet = [
    { nom: "Accueil", url: "/Accueil" },
    { nom: "Liste Clients", url: "/ListeClient" },
    { nom: "Liste Devis", url: "/ListeDevis" },
    { nom: "Stats", url: "/" },
  ];
  const [listDevis, setListDevis] = useState([]);
  const [listDevisMemo, setListDevisMemo] = useState([]);
  const [listEtape, setListEtape] = useState([]);
  const [etapeCherche, setEtapeCherche] = useState();
  const [etatCherche, setEtatCherche] = useState();
  const [numeroCherche, setNumeroCherche] = useState();
  const history = useHistory();
  const token = localStorage.getItem("token");
  const handleChangeNumeroDevis = (event) => {
    setListDevis(
      listDevisMemo.filter((devis) =>
        devis.numero_devis.toString().includes(event.target.value)
      )
    );
    setNumeroCherche(event.target.value);
    setEtapeCherche("");
    setEtatCherche("");
  };

  const handleChangeEtatDevis = (event) => {
    console.log(event.target.value);
    event.target.value
      ? setListDevis(
          listDevisMemo.filter((devis) => devis.etat === event.target.value)
        )
      : setListDevis(listDevisMemo);
    setEtatCherche(event.target.value);
    setEtapeCherche("");
    setNumeroCherche("");
  };

  const handleChangeEtapeDevis = (event) => {
    console.log(event.target.value);
    event.target.value
      ? setListDevis(
          listDevisMemo.filter(
            (devis) => devis.etape?.nom === event.target.value
          )
        )
      : setListDevis(listDevisMemo);
    setEtapeCherche(event.target.value);
    setEtatCherche("");
    setNumeroCherche("");
  };

  const DevisItems = ({ children }) =>
    children.map((value) => {
      return (
        <BlocDevis
          id={value.id}
          image={etatDevis[value.etat]}
          devis={value.numero_devis}
          etat={value.etat}
          nom={value.client.nom}
          prenom={value.client.prenom}
          ville={value.adresse.ville.nom}
        />
      );
    });

  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }
    console.log(props.location);
    const fetchData = async () => {
      const result = await axios.get(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListDevis(result.data);
      setListDevisMemo(result.data);

      const resultEtape = await axios.get(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/etape`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setListEtape(resultEtape.data);
    };
    fetchData();
  }, [history, props.location, token]);
  return (
    <Box height="100%" display="flex" flexDirection="column">
      <Navbar titre={onglet[2].nom} onglet={onglet} />
      <Grid h="200px" templateColumns="repeat(6, 1fr)" gap={4}>
        <GridItem colSpan={2}>
          <SimpleGrid columns={2} spacing={10} mt={10} ml={5}>
            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Numéro devis :{" "}
            </Text>

            <Input
              placeholder="Recherche par n°"
              size="md"
              w="100%"
              onChange={handleChangeNumeroDevis}
              value={numeroCherche}
            />

            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Etat devis :{" "}
            </Text>

            <Select
              placeholder="Select option"
              w="100%"
              onChange={handleChangeEtatDevis}
              value={etatCherche}
            >
              <EtatSelect>{etats}</EtatSelect>
            </Select>

            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Etapes devis :{" "}
            </Text>

            <Select
              placeholder="Select option"
              w="100%"
              onChange={handleChangeEtapeDevis}
              value={etapeCherche}
            >
              <EtapeSelect>{listEtape}</EtapeSelect>
            </Select>
          </SimpleGrid>
        </GridItem>
        <GridItem colSpan={4}>
          <Box mt={5}>
            <SimpleGrid
              columns={{ base: 1, md: 2, xl: 3 }}
              spacing={10}
              style={{ maxHeight: "85vh", overflow: "auto" }}
            >
              <DevisItems>{listDevis}</DevisItems>
            </SimpleGrid>
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
}
