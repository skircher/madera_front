import React, { useState, useEffect } from "react";
import {
  Box,
  Heading,
  SimpleGrid,
  Text,
  Button,
  Grid,
  GridItem,
  Image,
  useDisclosure,
} from "@chakra-ui/react";
import { AiOutlineRollback } from "react-icons/ai";
import { Link } from "react-router-dom";
import { RiImageEditLine } from "react-icons/ri";
import { AiOutlineReload } from "react-icons/ai";
import Navbar from "../Composant/Navbar";
import ListeElement from "../Composant/ListeElement";
import ModalChoixMaison from "../Modal/ModalChoixMaison";
import { useHistory } from "react-router-dom";
import axios from "axios";

export default function DetailProduit(props) {
  const history = useHistory();
  const token = localStorage.getItem("token");
  const colonneNoms = ["Famille", "Nom", "Dimension", "Quantité", "Prix"];
  const construction = "/images/construction.jpg";

  const [monProduit, setMonproduit] = useState([]);
  const [listModule, setListModule] = useState([]);
  const [devisSelect, setDevisSelect] = useState([]);
  const [client, setClient] = useState([]);
  const [adresse, setAdresse] = useState([]);
  const [gamme, setGamme] = useState([]);
  const [imageMaison, setImageMaison] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();

  let onglet = [
    { nom: "Accueil", url: "/Accueil" },
    {
      nom: "Détail Client",
      url: `/FicheClient/${client.id}`,
    },
  ];

  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }
    const fetchData = async () => {
      const result = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/produit/${props.match.params.idProduit}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setMonproduit(result.data);
      setListModule(result.data.moduleClients);
      setGamme(result.data.gamme);

      const resultDevis = await axios(
        `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis/${props.match.params.idDevis}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setDevisSelect(resultDevis.data);
      setClient(resultDevis.data.client);
      setAdresse(resultDevis.data.adresse);
    };
    fetchData();
    getPicture();
  }, [
    props.match.params.idProduit,
    props.match.params.idDevis,
    history,
    token,
  ]);

  let miseEnPageTableauModule = () => {
    let listModuleFormat = [];
    listModule.forEach((moduleClt) => {
      const detailListModule = {
        get_url: null,
        state: null,
        Famille: moduleClt.familleModule.nom,
        Nom: moduleClt.nom,
        Dimension: moduleClt.dimension,
        Quantité: 1,
        prix: calculPrixModule(moduleClt.composants),
      };
      let modulExist = listModuleFormat.find(
        (x) => x.Nom === detailListModule.Nom
      );
      if (modulExist) {
        modulExist.Quantité += 1;
      } else {
        listModuleFormat.push(detailListModule);
      }
    });
    return listModuleFormat;
  };

  let calculPrixModule = (listComposant) => {
    let prix = 0;
    listComposant.forEach((composant) => {
      prix += composant.prix;
    });
    return prix;
  };

  function refreshPage() {
    window.location.reload();
  }

  let tansmissionTokenApi = () => {
    var windowReference = window.open();
    axios
      .post(`${process.env.REACT_APP_URL_BACKEND}/api/v1/token`, token, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((result) => {
        windowReference.location = `${process.env.REACT_APP_URL_EDITOR}/editor/?produitId=${props.match.params.idProduit}&gamme=${monProduit.gamme.nom}&keyToken=${result.data}`;
      });
  };

  const getPicture = () => {
    var URL = `${process.env.REACT_APP_URL_BACKEND}/downloadProduit/${props.match.params.idProduit}_vue_trigo.jpg`;
    fetch(URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      if (response.status !== 404) {
        response.blob().then((blob) => {
          setImageMaison(window.URL.createObjectURL(blob));
        });
      } else {
        setImageMaison(construction);
      }
    });
  };

  return (
    <Box height="100%" display="flex" flexDirection="column">
      <Navbar titre={"Détail Produit"} onglet={onglet} />
      <Grid gap={2}>
        <GridItem colStart={0} colEnd={1}>
          <Box ml={5}>
            <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }}>
              Informations
            </Heading>
            <SimpleGrid
              columns={2}
              spacing={2}
              mt={5}
              ml={{ base: 2, md: 5, xl: 10 }}
              borderWidth="1px"
              borderRadius="lg"
              padding={5}
              backgroundColor="teal.100"
              color="teal.500"
              fontSize={{ base: "md", md: "md", xl: "lg" }}
            >
              <Text as="b">Nom : </Text>
              <Text as="i" color="grey">
                {client.nom}
              </Text>
              <Text as="b">Prénom : </Text>
              <Text as="i" color="grey">
                {client.prenom}
              </Text>
              <Text as="b">Adresse Chantier : </Text>
              <Text as="i" color="grey">
                {adresse.nom_adr}
              </Text>
              <Text as="b">Créé le : </Text>
              <Text as="i" color="grey">
                {monProduit.date_creation
                  ? monProduit.date_creation.split("T")[0]
                  : monProduit.date_creation}
              </Text>
              <Text as="b">Mis à jour le : </Text>
              <Text as="i" color="grey">
                {monProduit.date_maj
                  ? monProduit.date_maj.split("T")[0]
                  : monProduit.date_maj}
              </Text>
              <Text as="b">
                Devis n° :{" "}
                <Link to={`/DetailDevis/${devisSelect.id}`} as="i" color="grey">
                  <Button width="20%">
                    <AiOutlineRollback size="lg" />
                  </Button>
                </Link>
              </Text>
              <Text as="i" color="grey">
                {devisSelect.numero_devis}
              </Text>
              <Text as="b">Gamme : </Text>
              <Text as="i" color="grey">
                {gamme.nom}
              </Text>
              <Text as="b">CCTP : </Text>
              <Text as="i" color="grey">
                {"Plot Béton"}
              </Text>
            </SimpleGrid>
          </Box>
          <Box mt={5} ml={5}>
            <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }}>
              Vue Isométrique
            </Heading>
            <SimpleGrid
              columns={{ base: 1, md: 1, xl: 2 }}
              row={{ base: 2, md: 2, xl: 1 }}
            >
              <Box
                ml={{ base: 5, md: 5, xl: 15 }}
                mt={5}
                borderWidth="1px"
                borderRadius="lg"
                width={{ base: "100%", md: "90%", xl: "100%" }}
              >
                <Image src={imageMaison} alt="Segun Adebayo" />
              </Box>

              <Box
                ml={{ base: 5, md: 5, xl: 10 }}
                mt={{ base: 0, md: 0, xl: 10 }}
              >
                <SimpleGrid
                  columns={{ base: 2, md: 2, xl: 1 }}
                  row={{ base: 1, md: 1, xl: 2 }}
                >
                  <Button
                    mt={2}
                    rightIcon={<RiImageEditLine />}
                    colorScheme="teal"
                    onClick={
                      monProduit?.moduleClients?.length > 0
                        ? tansmissionTokenApi
                        : onOpen
                    }
                    fontSize={{ base: "xs", md: "sm", xl: "lg" }}
                  >
                    Editer le produit
                  </Button>
                  <ModalChoixMaison
                    isOpen={isOpen}
                    onClose={onClose}
                    produit={monProduit}
                  ></ModalChoixMaison>
                  <Button
                    mt={2}
                    rightIcon={<AiOutlineReload />}
                    colorScheme="teal"
                    onClick={refreshPage}
                    fontSize={{ base: "xs", md: "sm", xl: "lg" }}
                    ml={{ md: "2", xl: "0" }}
                  >
                    Reload
                  </Button>
                </SimpleGrid>
              </Box>
            </SimpleGrid>
          </Box>
        </GridItem>
        <GridItem colStart={1} colEnd={2}>
          <Heading fontSize={{ base: "md", md: "2xl", xl: "4xl" }} mb={5}>
            Liste des modules du produit
          </Heading>
          <ListeElement
            nomColonne={colonneNoms}
            data={miseEnPageTableauModule()}
            dessousListe={"Prix Total : " + monProduit.prix_total + " €"}
          />
        </GridItem>
      </Grid>
    </Box>
  );
}
