import React, { useEffect, useState } from "react";
import {
  Box,
  SimpleGrid,
  Grid,
  GridItem,
  Input,
  Text,
  Select,
} from "@chakra-ui/react";
import Navbar from "../Composant/Navbar";
import axios from "axios";
import BlocClient from "../Composant/BlocClient";
import { useHistory } from "react-router-dom";

export default function ListeClient(props) {
  const genreClient = {
    false: "/images/femme.png",
    true: "/images/homme.jpg",
  };
  const onglet = [
    { nom: "Accueil", url: "/Accueil" },
    { nom: "Liste Clients", url: "/ListeClient" },
    { nom: "Liste Devis", url: "/ListeDevis" },
    { nom: "Stats", url: "/" },
  ];
  const [listClient, setListClient] = useState([]);
  const [listClientMemo, setListClientMemo] = useState([]);
  const [listDevis, setListDevis] = useState([]);
  const history = useHistory();
  const token = localStorage.getItem("token");
  useEffect(() => {
    if (token === undefined || token === null) {
      history.push("/");
    }
    axios
      .get(`${process.env.REACT_APP_URL_BACKEND}/api/v1/client`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        let mesClients = response.data;
        mesClients.forEach((client) => {
          client.devis = getListDevisByClient(client.id);
        });
        setListClient(mesClients);
        setListClientMemo(mesClients);
      });
  }, [history, token]);

  function getListDevisByClient(client_id) {
    let devisList = [];
    axios(
      `${process.env.REACT_APP_URL_BACKEND}/api/v1/devis/client/${client_id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    ).then((response) => {
      response.data.forEach((element) => {
        devisList.push(element.numero_devis);
      });
      const listToAdd = [devisList];
      setListDevis(listToAdd);
    });
    return devisList;
  }

  const handleChangeNomClient = (event) => {
    setListClient(
      listClientMemo.filter((client) =>
        client.nom.toUpperCase().includes(event.target.value.toUpperCase())
      )
    );
  };

  const ClientItems = ({ children }) =>
    children.map((value, index) => {
      return (
        <BlocClient
          id={value.id}
          image={genreClient[value.genre]}
          nom={value.nom}
          prenom={value.prenom}
          ville={value.adresses[0].ville.nom}
          devis={value.devis}
        />
      );
    });
  return (
    <Box height="100%" display="flex" flexDirection="column">
      <Navbar titre={onglet[1].nom} onglet={onglet} />

      <Grid templateColumns="repeat(6, 1fr)" gap={4}>
        <GridItem colSpan={2}>
          <SimpleGrid
            columns={2}
            spacing={{ base: 5, md: 5, xl: 10 }}
            mt={10}
            ml={5}
          >
            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Nom client :
            </Text>

            <Input
              onChange={handleChangeNomClient}
              placeholder="Recherche par nom"
              size="md"
              w="100%"
            />

            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Etat devis :
            </Text>

            <Select placeholder="Select option" w="100%">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
            </Select>

            <Text fontSize={{ base: "xl", md: "xl", xl: "3xl" }}>
              Etapes devis :
            </Text>

            <Select placeholder="Select option" w="100%">
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
              <option value="option3">Option 3</option>
            </Select>
          </SimpleGrid>
        </GridItem>
        <GridItem colSpan={4} mr="5">
          <Box mt={5}>
            <SimpleGrid
              columns={2}
              spacing={10}
              style={{ maxHeight: "85vh", overflow: "auto" }}
            >
              <ClientItems>{listClient}</ClientItems>
            </SimpleGrid>
          </Box>
        </GridItem>
      </Grid>
    </Box>
  );
}
