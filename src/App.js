import { ChakraProvider } from "@chakra-ui/react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Authentification from "./Page/Authentification";
import Accueil from "./Page/Accueil";
import ListeDevis from "./Page/ListeDevis";
import DetailDevis from "./Page/DetailDevis";
import DetailProduit from "./Page/DetailProduit";
import FicheClient from "./Page/FicheClient";
import ListeClient from "./Page/ListeClient";

function App() {
  return (
    <Router>
      <ChakraProvider>
        <Route exact path="/" component={Authentification} />
        <Route exact path="/Accueil" component={Accueil} />
        <Route exact path="/ListeClient" component={ListeClient} />
        <Route exact path="/ListeDevis" component={ListeDevis} />
        {/* <Route exact path="/DetailDevis" component={DetailDevis} /> */}
        <Route path="/DetailDevis/:idDevis" component={DetailDevis} />
        <Route
          exact
          path="/DetailProduit/:idProduit/devis/:idDevis"
          component={DetailProduit}
        />
        {/* <Route exact path="/FicheClient" component={FicheClient} /> */}
        <Route path="/FicheClient/:idClient" component={FicheClient} />
      </ChakraProvider>
    </Router>
  );
}

export default App;
